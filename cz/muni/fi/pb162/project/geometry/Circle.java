package cz.muni.fi.pb162.project.geometry;


/**
 * Clas representing a circle with center point and radius
 * 
 * @author Stanislav Marek
 * @version 27.10.2013
 */
public class Circle extends GeneralRegularPolygon implements Solid{

    private double radius;
    private static final Vertex2D UNI_CENTER = new Vertex2D(0, 0);
    private static final double UNI_RADIUS = 1.0;
     
    /**
     * Constructor without parametres, creates circle centered at [0,0] with radius = 1 
     */
    public Circle(){
        super(UNI_CENTER, Integer.MAX_VALUE, 0.0);
        this.radius = UNI_RADIUS;        
    }
    
    /**
     * Constructor for objects of class Circle with given parametres
     * 
     * @param otherCenter   vertex representing center of the circle
     * @param otherRadius   value representing radius of the circle
     */
    public Circle(Vertex2D center, double radius){
        super(center, Integer.MAX_VALUE, 0.0);
        this.radius = radius;
    }

    public double getLength(){
        return 2 * Math.PI * radius;
    }
/*   
    public double getWidth(){
        return 2 * radius;
    }
    
    public double getHeight(){
        return 2 * radius;
    }
*/
    public double getArea(){
        return Math.PI * radius * radius;
    }

    public double getRadius(){
        return radius;
    }
    public String toString(){
        return "Circle: center=" + getCenter() + ", radius=" + radius;
    }
}
