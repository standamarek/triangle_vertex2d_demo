package cz.muni.fi.pb162.project.geometry;


/**
 * Class gauger
 * 
 * @author Stanislav Marek 
 * @version 21.10.2013
 */
public class Gauger{
    

    /**
     * printMeasurement for measurable objects, prints properties of given object (vertices that forms the object + length/perimeter)
     * 
     * @param  myObject  Measurable object with properties we want to print
     */
    public static void printMeasurement(Measurable myObject){
        System.out.println(myObject.toString());
        System.out.println("Length/Perimeter: " + myObject.getLength());        
    }
    
    /**
     * printMeasurement for solid objects, prints properties of given object  (vertices that forms the object + length/perimeter + area)
     * 
     * @param  myObject  Solid object with properties we want to print
     */
    public static void printMeasurement(Solid myObject){
        System.out.println(myObject.toString());
        System.out.println("Length/Perimeter: " + myObject.getLength());
        System.out.println("Area: " + myObject.getArea());        
    }
}
