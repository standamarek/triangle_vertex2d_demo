package cz.muni.fi.pb162.project.geometry;


/**
 * Class ArrayPolygon represents array of vertices creating polygon
 * 
 * @author Stanislav Marek 
 * @version 11.11.2013
 */
public class ArrayPolygon extends SimplePolygon{
    // instance variables - replace the example below with your own
    private Vertex2D[] points;

    /**
     * Constructor for objects of class ArrayPolygon with null arrays check
     */
    public ArrayPolygon(Vertex2D... points) throws NullPointerException{
        if(points == null)
            throw new NullPointerException("NULL array");
        else{
            for(int i = 0; i < points.length; i++){
                if(points[i] == null)
                    throw new NullPointerException("NULL array element at index " + i);
                }
        
            this.points = new Vertex2D[points.length];
            this.points = points.clone();
        }
    }
    
    public Vertex2D getVertex(int index) throws IllegalArgumentException{
        if(index < 0){
            throw new IllegalArgumentException("Trying to set negative index in array");
        }
        else{
            return points[index % points.length];
        }
    }

    public int getNumVertices(){
        return points.length;
    }
}
