package cz.muni.fi.pb162.project.geometry;
import java.util.*;


/**
 * Write a description of class VertexInverseComparator here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class VertexInverseComparator implements Comparator<Vertex2D>
{
   public int compare(Vertex2D vert1, Vertex2D vert2){
       return vert2.compareTo(vert1);
    }
}
    