package cz.muni.fi.pb162.project.geometry;


/**
 * Enumeration class Color - basic colors enumeration
 * 
 * @author Stanislav Marek
 * @version 26.11.2013
 */
public enum Color
{
    RED, GREEN, BLUE, YELLOW, BROWN, BLACK, WHITE
}
