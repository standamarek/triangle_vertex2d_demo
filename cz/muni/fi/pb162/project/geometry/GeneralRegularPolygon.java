 package cz.muni.fi.pb162.project.geometry;


/**
 * Class representing GeneralRegularPolygon and implementing methods to calculate certain properties
 * 
 * @author Stanislav Marek 
 * @version 26.11.2013
 */
public class GeneralRegularPolygon implements RegularPolygon, Colored{
    private Vertex2D center;
    private int numEdges;
    private double edgeLength;
    private Color color;

    /**
     * Constructor for objects of class GeneralRegularPolygon with parametres
     * @param center center of given polygon
     * @param numEdges number of edges
     * @param edgeLength edge length
     */
    public GeneralRegularPolygon(Vertex2D center, int numEdges, double edgeLength){
        this.center = center;
        this.numEdges = numEdges;
        this.edgeLength = edgeLength;
        setColor(Color.BLACK);
    }

    public double getRadius(){
        double radius;
        radius = edgeLength / (2 * Math.sin(Math.PI/numEdges)); // formula to calculate radius of general polygon
        return radius;
    }
    
    public Vertex2D getCenter(){
        return center;
    }
    
    public int getNumEdges(){
        return numEdges;
    }
    
    public double getEdgeLength(){
        return edgeLength;
    }
    
    public double getLength(){
        return numEdges * edgeLength;
    }
    
    public double getHeight(){
        return 2 * getRadius();
    }
    
    public double getWidth(){
        return 2 * getRadius();
    }
    
    public double getArea(){
        return (numEdges * Math.pow(getRadius(), 2) * Math.sin(2 * Math.PI / numEdges)) / 2; // formula to calculate area of general polygon       
    }
    
    public void setColor(Color color){
        this.color = color;
    }
    
    public Color getColor(){
        return color;
    }
    
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public String toString(){
        return numEdges + "-gon: center=" + center.toString() +", edge length=" + edgeLength + ", color=" + color;        
    }
}
