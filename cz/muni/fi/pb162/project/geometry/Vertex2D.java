package cz.muni.fi.pb162.project.geometry;


/**
 * Class representing vertex in 2D
 * 
 * @author Stanislav Marek 
 * @version 9.12.2013
 */
public class Vertex2D implements Comparable<Vertex2D>
{
    // coord x
    private double x;

    // coord y
    private double y;

    public Vertex2D(double x, double y){
        this.x = x;
        this.y = y;
    }
    
    /**
     * Method that calculates distance from one vertex to another
     * 
     * @param  other   Other vertex to calculate distance to 
     * @return     distance from vertex to given other vertex
     */
    public double distance(Vertex2D other)
    {
        if(other == null){
            return -1.0; // returns nonsense when other vertex points to null
        }
        return Math.sqrt(Math.pow((other.x - x), 2.0) + Math.pow((other.y - y), 2.0)); // formula to calculate distance between two vertices
    }
    
    @Override public int hashCode(){
        int hash = 1;
        hash = hash * 113 + (int)x;
        hash = hash * 73 + (int)y;
        
        return hash;
    }
    
    /**
     * Method that compares two points
     * 
     * @param point Point to be compared
     * @return returns true if points are equal and false if they're not
     */
    @Override public boolean equals(Object vert){
        if (vert instanceof Vertex2D){
            Vertex2D point = (Vertex2D)vert;
            return (Math.abs(this.x - point.getX()) < Triangle.EPS) && (Math.abs(this.y - point.getY()) < Triangle.EPS);
        }
        else
            return false;
    }
    
    public int compareTo(Vertex2D vert){
        int value = (int)Math.signum(x - vert.getX());
        if (value != 0) {
            return value;
        }
        return (int)Math.signum(y - vert.getY());
    }
 
    public double getX()
    {
        return x;
    }
    public double getY()
    {
        return y;
    }
    
    public String toString()
    {
        return "[" + x + ", " + y + "]";
    }
}
