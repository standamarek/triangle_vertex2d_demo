package cz.muni.fi.pb162.project.geometry;

import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Collections;

/**
 * Class CollectionPolygon represents array of vertices creating polygon
 * 
 * @author Stanislav Marek 
 * @version 11.11.2013
 */
public class CollectionPolygon extends SimplePolygon{
    // instance variables - replace the example below with your own
    private List<Vertex2D> listedPoints = new ArrayList<Vertex2D>();

    /**
     * Constructor for objects of class ArrayPolygon with null arrays check
     */
    public CollectionPolygon(Vertex2D... points) throws NullPointerException{
        if(points == null)
            throw new NullPointerException("NULL array");
        else{
            for(int i = 0; i < points.length; i++){
                if(points[i] == null)
                    throw new NullPointerException("NULL array element at index " + i);
                listedPoints.add(points[i]);
                }
        }
    }
    
    public Collection<Vertex2D> getVertices(){
        return Collections.unmodifiableList(listedPoints);
    }
    
    /**
     * Method invert - inverts order of polygon vertices
     * 
     * @return polygon with inverted order of vertices
     */
    public CollectionPolygon invert(){
        int size = listedPoints.size();
        Vertex2D[] points = new Vertex2D[size];
        for(int i = 0; i < size; i++){
            points[i] = listedPoints.get(size - (i + 1));
        }
        return new CollectionPolygon(points);
    }
    
    /**
     * Method getVertex returns desired vertex
     * 
     * @param index index of desired vertex
     * @return vertex with given index
     */
    public Vertex2D getVertex(int index) throws IllegalArgumentException{
        if(index < 0){
            throw new IllegalArgumentException("Trying to set negative index in array");
        }
        else{
            return listedPoints.get(index % listedPoints.size());
        }
    }

    public int getNumVertices(){
        return listedPoints.size();
    }
}
