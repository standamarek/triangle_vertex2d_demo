package cz.muni.fi.pb162.project.geometry;
import java.util.Map;
import java.util.TreeMap;
import java.util.Set;
import java.util.Collections;
import java.util.Collection;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;

/**
 * Polygons stored with names
 * 
 * @author Stanislav Marek
 * @version 17.12.2013
 */
public class LabeledPolygon extends SimplePolygon implements PolygonIO{
    private Map<String, Vertex2D> vertices = new TreeMap<>();
        
    public void addVertex(String label, Vertex2D vert){
        if(label == null){
            throw new NullPointerException("Label is null");
        }
        if(vert == null){
            throw new NullPointerException("Vertex is null");
        }
       
        vertices.put(label, vert);
    }
    
    public Vertex2D getVertex(String label) throws IllegalArgumentException{
        if(!(vertices.containsKey(label))){
            throw new IllegalArgumentException("Vertex with that label doesn't exist");
        }
        return vertices.get(label);
    }
    
    public Vertex2D getVertex(int index) throws IllegalArgumentException{
        
        int i = 0;
        index = index % getNumVertices();
        
        if(index < 0){
            throw new IllegalArgumentException("Trying to get vertex with negative index");
        }
        for(String str : vertices.keySet()){   
            if(i == index){
                return vertices.get(str);
            }
            i++;
        }
        return null;
    }
    
    public int getNumVertices(){
        return vertices.size();
    }
    
    
    public Collection<Vertex2D> getSortedVertices(){
        SortedSet<Vertex2D> sortedVertices = new TreeSet<>();
        for(Vertex2D vertex : vertices.values()){
            sortedVertices.add(vertex);
        }
        return Collections.unmodifiableSortedSet(sortedVertices);
    }
    
    public Collection<Vertex2D> getSortedVertices(Comparator<Vertex2D> comparator){
        SortedSet<Vertex2D> sortedVertices = new TreeSet<Vertex2D>(comparator);
        for(Vertex2D vertex : vertices.values()){
            sortedVertices.add(vertex);
        }
        return Collections.unmodifiableSortedSet(sortedVertices);
    }
    
    public Set<String> getLabels()
    {
        return vertices.keySet();
    }

    
    
    /**
     * Method write - writes stored vertices to given output stream
     * 
     * @param outStream - output stream
     */
    public void write(OutputStream outStream) throws IOException{
        String label;
        Vertex2D vert;
        BufferedOutputStream buffOut = new BufferedOutputStream(outStream);
        StringBuilder outString = new StringBuilder();
     
        for(Map.Entry<String,Vertex2D> entry : vertices.entrySet()) {
            vert = entry.getValue();
            label = entry.getKey();
            outString.append(String.valueOf(vert.getX()) + " " + String.valueOf(vert.getY()) + " " + label);
            buffOut.write(outString.toString().getBytes());
            buffOut.write(System.lineSeparator().getBytes());
            buffOut.flush();
        }
         
        buffOut.close();
    }
    
    public void write(File file) throws IOException{
        OutputStream outStream = new FileOutputStream(file);
        write(outStream);        
    }
    
    /**
     * Method read - reads vertices from given stream and stores them
     * 
     * @param inStream - input stream
     */
    public void read(InputStream inStream) throws IOException{
        BufferedReader buffReader = new BufferedReader(new InputStreamReader(inStream));
        String line = buffReader.readLine();
        TreeMap<String, Vertex2D> verts = new TreeMap<>();  
        
               
        try {    
            while(line != null)
            {
                String[] parsedVerts = new String[3];
                parsedVerts = line.split(" ",3);
                String separator = parsedVerts[2];
                Vertex2D vertex = new Vertex2D(Double.parseDouble(parsedVerts[0]),Double.parseDouble(parsedVerts[1]));
               
                verts.put(separator,vertex);
               
                line = buffReader.readLine();
            }
            vertices.putAll(verts);
        }
        catch(NumberFormatException ex)
        {
            throw new IOException("Reading error", ex);
        }
        finally {
            buffReader.close();
        }
        
        
    }
    
    public void read(File file) throws IOException{
        InputStream inStream = new FileInputStream(file);
        read(inStream);
    }

}
