package cz.muni.fi.pb162.project.geometry;



/**
 * Abstract class SimplePolygon - represents polygon and implements methods to retrieve it's properties
 * 
 * @author Stanislav Marek 
 * @version 11.11.2013
 */
public abstract class SimplePolygon implements Polygon{

    public double getArea(){
        int numVertices = getNumVertices();
        double area = 0;
        for(int i = 0; i < (numVertices - 1); i++){
            Vertex2D actual = getVertex(i);
            Vertex2D actualPlusOne = getVertex(i + 1);
            area += actual.getX() * actualPlusOne.getY() - actualPlusOne.getX() * actual.getY();
        }
            area += getVertex(numVertices-1).getX() * getVertex(0).getY() - getVertex(0).getX() * getVertex(numVertices-1).getY();
        return area/2;      
    }
    public double getWidth(){
        double minX = Long.MAX_VALUE;
        double maxX = Long.MIN_VALUE;
        int numVertices = getNumVertices();
        for(int i = 0; i < numVertices; i++){
            double x = getVertex(i).getX();
            minX = Math.min(minX, x);
            maxX = Math.max(maxX, x);
        }
        return maxX - minX;
    }
    public double getHeight(){
        double minY = Long.MAX_VALUE;
        double maxY = Long.MIN_VALUE;
        int numVertices = getNumVertices();
        for(int i = 0; i < numVertices; i++){
            double y = getVertex(i).getY();
            minY = Math.min(minY, y);
            maxY = Math.max(maxY, y);
        }
        return maxY - minY;
    }
    public double getLength(){
        int numVertices = getNumVertices();
        double perimeter = 0;
        for(int i = 0; i < numVertices; i++){
            perimeter += Math.abs(getVertex(i).distance(getVertex(i + 1)));
        }
        perimeter += Math.abs(getVertex(numVertices).distance(getVertex(0)));
        return perimeter;
    }
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y    a sample parameter for a method
     * @return        the sum of x and y 
     */
    public String toString(){
        String s = "Polygon: vertices = ";
        int numVertices = getNumVertices();
        for(int i = 0; i < numVertices; i++){
            s += getVertex(i).toString();
            if(i + 1 != numVertices)
                s += " ";
        }
        return s;
    }
}
