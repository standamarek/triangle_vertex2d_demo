package cz.muni.fi.pb162.project.geometry;


/**
 * Class Triangle. Creates triangle made of three Vertex2D objects.
 * 
 * @author Stanislav Marek 
 * @version 11.11.2013
 */
public class Triangle extends ArrayPolygon implements Solid{

    public static final double EPS = 0.001;
    
    
    public Triangle(Vertex2D vertexA, Vertex2D vertexB, Vertex2D vertexC)
    {
        super(vertexA, vertexB, vertexC);
    }

    /**
     * Method that calculates lenghts of the edges and returns whether the triangle is equilateral or not
     * 
     * @return     true if the triangle is equilateral, false when it isn't, substracting lenghts of edges calculated using method distance from of class Vertex2D
     *             and checking whether they are equal or not (they are double, so we can't use == 0, we have to check it as below)
     */
    public boolean isEquilateral() {
        Vertex2D vertexA = getVertex(0);
        Vertex2D vertexB = getVertex(1);
        Vertex2D vertexC = getVertex(2);
        return Math.abs(vertexA.distance(vertexB) - vertexB.distance(vertexC)) < EPS && Math.abs(vertexA.distance(vertexB) - vertexC.distance(vertexA)) < EPS;
    }
    

}
