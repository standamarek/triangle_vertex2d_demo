package cz.muni.fi.pb162.project.geometry;


/**
 * Class creating regular octagon
 * 
 * @author Stanislav Marek 
 * @version 27.10.2013
 */
public class RegularOctagon extends GeneralRegularPolygon{

    /**
     * Constructor for objects of class RegularOctagon
     */
    public RegularOctagon(Vertex2D center, double egdeLength)
    {
        super(center, 8, egdeLength);
    }
}
