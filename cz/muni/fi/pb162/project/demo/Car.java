package cz.muni.fi.pb162.project.demo;


/**
 * Write a description of class Car here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Car
{
    // instance variables - replace the example below with your own
    private int x;

    /**
     * Constructor for objects of class Car
     */
    public Car(int x)
    {
        this.x = x;
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public String toString()
    {
        return "Auto: " + x;
    }
    public void setX(int x)
    {
        this.x = x;
    }
}
