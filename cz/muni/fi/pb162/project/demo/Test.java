package cz.muni.fi.pb162.project.demo;


/**
 * Write a description of class Test here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Test
{
    /**
     * Constructor for objects of class Test
     */
    public static void main(String[] args)
    {
        int a = 5;
        System.out.println("Hodnota a:" + a);
        int b = 3;
        System.out.println("Hodnota b:" + b);
        a = b;
        System.out.println("Hodnota a:" + a);
        System.out.println("Hodnota b:" + b);
        a++;
        System.out.println("Hodnota a:" + a);
        System.out.println("Hodnota b:" + b);
        
        Car c = new Car(1);
        System.out.println(c.toString());
        Car d = new Car(2);
        System.out.println(d.toString());
        
        c = d;
        System.out.println(c.toString());
        System.out.println(c.toString());
                
        c.setX(3);
        System.out.println(c.toString());
        System.out.println(c.toString());
    }
}
