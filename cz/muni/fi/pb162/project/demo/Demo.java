package cz.muni.fi.pb162.project.demo;
import cz.muni.fi.pb162.project.geometry.*;
import static org.junit.Assert.*;
import org.junit.Test;


/**
 * Demo trida, demonstruje funkcnost implementace trid Triangle a Vertex2D.
 * 
 * @author Stanislav Marek
 * @version 15.10.2013
 */
public class Demo
{
    /**
     * Hlavni spustitelna metoda, nastavuje body pro jednotlive strany trojuhelniku a pote jej
     * vykresli a vypise souradnice vrcholu.
     * @param args pole stringu
     */

    public static void main(String[] args)
    {        
        
        
        Vertex2D v1 = new Vertex2D(1.01, -1.01);
        Vertex2D v2 = new Vertex2D(1.01, -1.01);
        Vertex2D v3 = new Vertex2D(1.0, -1.0);
        
        assertTrue("Spatna implementace rovnosti -- porovnani na sebe sama", v1.equals(v1));
        assertTrue("Spatna implementace rovnosti -- porovnani se stejnym vrcholem", v1.equals(v2));
        assertFalse("Spatna implementace rovnosti -- porovnani s odlisnym vrcholem", v1.equals(v3));
    }

}
