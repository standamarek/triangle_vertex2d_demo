package cz.muni.fi.pb162.project.db;

import java.net.NoRouteToHostException;
import java.net.UnknownHostException;
import java.io.IOException;

/**
 * Write a description of class MyStorage here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyStorage implements Storage
{
    /**
     * Constructor for objects of class MyStorage
     */
    private Connector connector;
    private int maxAttempts;
    
    public MyStorage(Connector connector, int maxAttempts)
    {
        if(connector == null){
            throw new IllegalArgumentException("connector");
        }
        else if(maxAttempts < 1){
            throw new IllegalArgumentException("maxAttempts");
        }
        else {
            this.connector = connector;
            this.maxAttempts = maxAttempts;
        }
    }
    
    public void store(String host, Object data) throws DbUnreachableException, CannotStoreException{
        if(data == null){
            throw new IllegalArgumentException("data");
        }
        Connection connection = null;
        try {
            connection = connector.getConnection(host);
        } catch (UnknownHostException|NoRouteToHostException e) {
            throw new DbUnreachableException("Unable to reach connection", e);
        }
    //try to sendData until maxAttempts
        while(maxAttempts > -1){
            try {
                connection.sendData(data);
            } catch(IOException e){
                maxAttempts--;
                if(maxAttempts == 0){
                    throw new CannotStoreException("Cannot save data", e);
                }
                continue;
            }
            break;
        }


    }
}


