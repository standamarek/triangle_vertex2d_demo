package cz.muni.fi.pb162.project.db;


/**
 * Write a description of class DbUnreachableException here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DbUnreachableException extends DbException{    
    public DbUnreachableException(String message){
        super(message);
    }
    public DbUnreachableException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
