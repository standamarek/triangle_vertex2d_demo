package cz.muni.fi.pb162.project.db;


/**
 * Write a description of class CannotStoreException here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CannotStoreException extends DbException{
    public CannotStoreException(String message){
        super(message);
    }
    public CannotStoreException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
