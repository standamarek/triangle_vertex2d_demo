package cz.muni.fi.pb162.project.db;


/**
 * Write a description of class DbException here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DbException extends Exception{     
    public DbException(String message){
        super(message);
    }
    public DbException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
